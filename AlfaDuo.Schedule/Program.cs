using AlfaDuo.Core.Adapters;
using AlfaDuo.Core.Interfaces;
using AlfaDuo.Domain.Configuration;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories;
using AlfaDuo.Motor.Core;
using AlfaDuo.Motor.Interfaces;
using AlfaDuo.Schedule.Interfaces;
using AlfaDuo.Schedule.Workers;
using AlfaDuo.Services.DTO;
using AlfaDuo.Shared.Dto;
using AutoMapper;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

var CONN_STRING = builder.Configuration.GetConnectionString("DefaultConnection");

GlobalDiagnosticsContext.Set("configDir", "\\Logs");
GlobalDiagnosticsContext.Set("connectionString", CONN_STRING);
var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

try
{
    // Add services to the container.
    builder.Services.AddControllers();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();


    #region Repositories
    builder.Services.AddScoped<IIntegrationRepository, IntegrationRepository>();
    builder.Services.AddScoped<IPhaseRepository, PhaseRepository>();
    builder.Services.AddScoped<IMotorCore, MotorCore>();
    builder.Services.AddScoped<ISchedulerRepository, SchedulerRepository>();
    builder.Services.AddScoped<IPhaseRepository, PhaseRepository>();
    builder.Services.AddScoped<IRuleRepository, RuleRepository>();
    builder.Services.AddScoped<ISchedulerRunnerRepository, SchedulerRunnerRepository>();
    #endregion

    #region DataBase
    builder.Services.AddDbContext<RuleContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<PhaseContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<IntegrationContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<SchedulerContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<SchedulerRunnerContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    #endregion

    #region AutoMapper
    var autoMapperConfig = new MapperConfiguration(cfg =>
    {
        cfg.CreateMap<RuleDTO, Rule>().ReverseMap();
        cfg.CreateMap<Rule, RuleDTO>().ReverseMap();
        cfg.CreateMap<PhaseDto, Phase>().ReverseMap();
        cfg.CreateMap<Phase, PhaseDto>().ReverseMap();
    });

    builder.Services.AddSingleton(autoMapperConfig.CreateMapper());
    #endregion

    #region Hangfire
    builder.Services.AddHangfire(configuration =>
    {
        configuration.UseMemoryStorage();
        configuration.SetDataCompatibilityLevel(CompatibilityLevel.Version_170);
        configuration.UseSimpleAssemblyNameTypeSerializer();
        configuration.UseRecommendedSerializerSettings();
    });

    builder.Services.AddHangfireServer();
    #endregion

    #region WorkerConfiguration
    builder.Services.AddScoped<IGeneralWorker, GeneralWorker>();
    #endregion

    #region Services
    builder.Services.AddScoped<IHanaAdapter, HanaAdapter>();
    builder.Services.AddScoped<IServiceLayerAdapter, ServiceLayerAdapter>();
    builder.Services.AddScoped<IHttpAdapter, HttpAdapter>();
    #endregion

    #region Configure
    builder.Services.Configure<Configuration>(configuration.GetSection("AppConfig"));
    #endregion

    #region NLog
    builder.Logging.ClearProviders();
    builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
    builder.Host.UseNLog();
    #endregion

    #region Cache
    builder.Services.AddDistributedMySqlCache(options =>
    {
        options.ConnectionString = CONN_STRING;
        options.SchemaName = "alfa_duo";
        options.TableName = "api_auth_cache";
    });
    #endregion

    using (var app = builder.Build())
    {
        app.UseStaticFiles();
        app.UseHangfireDashboard();

        #region [ Scheduler ]

        //Cron.Minutely()
        RecurringJob.AddOrUpdate<IGeneralWorker>("General Worker", job => job.ExecuteAsync(null), "*/2 * * * *");
        #endregion

        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();

        await app.RunAsync();
    }
}
catch (Exception exception)
{
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    NLog.LogManager.Shutdown();
}