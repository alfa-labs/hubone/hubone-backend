﻿using Hangfire.Server;

namespace AlfaDuo.Schedule.Interfaces
{
    public interface IWorker
    {
        Task<bool> ExecuteAsync(PerformContext context);
    }
}
