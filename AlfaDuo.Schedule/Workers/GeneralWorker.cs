﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Motor.Interfaces;
using AlfaDuo.Schedule.Interfaces;
using Hangfire;
using Hangfire.Common;
using Hangfire.Server;
using System.Linq.Expressions;

namespace AlfaDuo.Schedule.Workers
{
    public class GeneralWorker : IGeneralWorker
    {
        private readonly ILogger<GeneralWorker> _logger;
        private readonly ISchedulerRepository _schedulerRepository;
        private readonly ISchedulerRunnerRepository _schedulerRunnerRepository;
        private readonly IMotorCore _motorCore;

        public GeneralWorker(
            ILogger<GeneralWorker> logger,
            ISchedulerRepository schedulerRepository,
            ISchedulerRunnerRepository schedulerRunnerRepository,
            IMotorCore motorCore)
        {
            _logger = logger;
            _schedulerRepository = schedulerRepository;
            _schedulerRunnerRepository = schedulerRunnerRepository;
            _motorCore = motorCore;
        }

        private async Task<IList<Scheduler>> GetAllSchedulers()
        {
            try
            {
                Expression<Func<Scheduler, bool>> filter = scheduler
                  => scheduler.Active;

                var schedulers = await _schedulerRepository.SearchAsync(filter);

                return schedulers;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao obter a listagem de Schedulers", ex);
                throw;
            }
        }

        private async Task<IList<SchedulerRunner>> GetScheduleInitialized()
        {
            try
            {
                var runners = await _schedulerRunnerRepository.GetAllAsync();
                return runners;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao obter a listagem de schedules inicializados", ex);
                throw;
            }
        }

        private async Task<bool> InitializeScheduler(Scheduler schedule)
        {
            try
            {
                _logger.LogInformation($"Inicializando o schedule {schedule.Id} - {schedule.Description}");

                var manager = new RecurringJobManager();

                manager.AddOrUpdate(schedule.Description, Job.FromExpression(() => _runJobScheduled(schedule.RuleId)), schedule.Cron);

                SchedulerRunner schedulerRunner = new SchedulerRunner();
                schedulerRunner.SetScheduleId(schedule.Id);
                schedulerRunner.SetStartAt(DateTime.Now);

                await _schedulerRunnerRepository.CreateAsync(schedulerRunner);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao inicializar Scheduler", ex);
                await _cleanScheduleRunningTable();
                return false;
            }
        }

        public async Task _runJobScheduled(long ruleId)
        {
            await _motorCore.ProcessByRule(ruleId);
        }

        private async Task _cleanScheduleRunningTable()
        {
            try
            {
                await _schedulerRunnerRepository.RemoveAllAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao limpar a tabela de schedule", ex);
            }

        }

        public async Task<bool> ExecuteAsync(PerformContext context)
        {
            try
            {
                DateTime date = DateTime.Now;

                Console.WriteLine($"Entrando no método as {date}" );
               
                await _cleanScheduleRunningTable();

                var allSchedules = await GetAllSchedulers();
                var scheduleInitialized = await GetScheduleInitialized();

                var arrayScheduleInitialized = scheduleInitialized.ToArray();

                foreach (Scheduler schedule in allSchedules)
                {
                    var index = Array.IndexOf(
                        arrayScheduleInitialized,
                        scheduleInitialized.Where(x => x.SchedulerId == schedule.Id).FirstOrDefault());

                    if (index == -1) await InitializeScheduler(schedule);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao realizar processo", ex);
                await _cleanScheduleRunningTable();
                throw;
            }
        }
    }
}
