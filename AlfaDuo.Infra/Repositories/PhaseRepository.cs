﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories.BaseRepository;

namespace AlfaDuo.Infra.Repositories
{
    public class PhaseRepository : BasePhaseRepository<Phase>, IPhaseRepository
    {
        private readonly PhaseContext _context;

        public PhaseRepository(PhaseContext context) : base(context)
        {
            _context = context;
        }

    }
}
