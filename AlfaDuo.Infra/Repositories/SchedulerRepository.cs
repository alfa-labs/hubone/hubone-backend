﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories.BaseRepository;

namespace AlfaDuo.Infra.Repositories
{
    public class SchedulerRepository : BaseSchedulerRepository<Scheduler>, ISchedulerRepository
    {
        private readonly SchedulerContext _context;

        public SchedulerRepository(SchedulerContext context) : base(context)
        {
            _context = context;
        }

    }
}
