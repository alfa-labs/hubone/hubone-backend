﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories.BaseRepository;

namespace AlfaDuo.Infra.Repositories
{
    public class SchedulerRunnerRepository : BaseSchedulerRunnerRepository<SchedulerRunner>, ISchedulerRunnerRepository
    {
        private readonly SchedulerRunnerContext _context;

        public SchedulerRunnerRepository(SchedulerRunnerContext context) : base(context)
        {
            _context = context;
        }

    }
}
