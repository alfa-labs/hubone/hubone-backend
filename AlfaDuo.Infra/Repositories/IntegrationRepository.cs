﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories.BaseRepository;

namespace AlfaDuo.Infra.Repositories
{
    public class IntegrationRepository : BaseIntegrationRepository<Integration>, IIntegrationRepository
    {
        private readonly IntegrationContext _context;

        public IntegrationRepository(IntegrationContext context) : base(context)
        {
            _context = context;
        }

    }
}
