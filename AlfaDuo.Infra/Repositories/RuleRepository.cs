﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories.BaseRepository;

namespace AlfaDuo.Infra.Repositories
{
    public class RuleRepository : BaseRuleRepository<Rule>, IRuleRepository
    {
        private readonly RuleContext _context;

        public RuleRepository(RuleContext context) : base(context)
        {
            _context = context;
        }

    }
}
