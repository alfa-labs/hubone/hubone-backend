﻿using AlfaDuo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AlfaDuo.Infra.Mappings
{
    public class RuleMap : IEntityTypeConfiguration<Rule>
    {
        public void Configure(EntityTypeBuilder<Rule> builder)
        {
            builder.ToTable("Rule");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.Type)
                .IsRequired()
                .HasMaxLength(1)
                .HasColumnName("type")
                .HasColumnType("VARCHAR(1)");

            builder.Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnName("description")
                .HasColumnType("VARCHAR(100)");

            builder.Property(x => x.Verb)
                .IsRequired(false)
                .HasMaxLength(5)
                .HasColumnName("verb")
                .HasColumnType("VARCHAR(5)");

            builder.Property(x => x.Endpoint)
                .IsRequired(false)
                .HasMaxLength(80)
                .HasColumnName("endpoint")
                .HasColumnType("VARCHAR(80)");

            builder.Property(x => x.Version)
                .IsRequired()
                .HasMaxLength(4)
                .HasColumnName("version")
                .HasColumnType("VARCHAR(4)");

            builder.Property(x => x.Active)
                .IsRequired()
                .HasColumnName("active")
                .HasColumnType("bit");

            builder.Property(x => x.Synchronous)
                .IsRequired()
                .HasColumnName("synchronous")
                .HasColumnType("bit");

            builder.Property(x => x.Rules)
                .IsRequired()
                .HasColumnName("rules")
                .HasColumnType("JSON");
        }
    }
}
