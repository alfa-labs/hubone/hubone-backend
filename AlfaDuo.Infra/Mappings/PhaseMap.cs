﻿using AlfaDuo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AlfaDuo.Infra.Mappings
{
    internal class PhaseMap : IEntityTypeConfiguration<Phase>
    {
        public void Configure(EntityTypeBuilder<Phase> builder)
        {
            builder.ToTable("Phase");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.IntegrationId)
                .IsRequired()
                .HasColumnName("integrationId")
                .HasColumnType("BIGINT");

            builder.Property(x => x.Sequence)
                .IsRequired()
                .HasColumnName("sequence")
                .HasColumnType("INTEGER");

            builder.Property(x => x.Type)
                .IsRequired()
                .HasColumnName("type")
                .HasColumnType("VARCHAR(1)");

            builder.Property(x => x.Rule)
                .IsRequired()
                .HasColumnName("rule")
                .HasColumnType("JSON");

            builder.Property(x => x.PhaseResult)
                .IsRequired(false)
                .HasColumnName("phaseResult")
                .HasColumnType("JSON");
        }
    }
}
