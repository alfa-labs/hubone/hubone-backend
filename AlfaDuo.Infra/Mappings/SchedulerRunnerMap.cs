﻿using AlfaDuo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AlfaDuo.Infra.Mappings
{
    public class SchedulerRunnerMap : IEntityTypeConfiguration<SchedulerRunner>
    {
        public void Configure(EntityTypeBuilder<SchedulerRunner> builder)
        {
            builder.ToTable("SchedulerRunner");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.SchedulerId)
                .HasColumnName("schedulerId")
                .HasColumnType("BIGINT");

            builder.Property(x => x.StartAt)
                .HasColumnName("startAt")
                .HasColumnType("TIMESTAMP");
        }
    }
}
