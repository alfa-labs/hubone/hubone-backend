﻿using AlfaDuo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace AlfaDuo.Infra.Mappings
{
    public class IntegrationMap : IEntityTypeConfiguration<Integration>
    {
        public void Configure(EntityTypeBuilder<Integration> builder)
        {
            builder.ToTable("Integration");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.RuleId)
                .IsRequired()
                .HasColumnName("RuleId")
                .HasColumnType("BIGINT");

            builder.Property(x => x.Status)
                .IsRequired()
                .HasColumnName("status")
                .HasColumnType("VARCHAR(10)");

            builder.Property(x => x.Payload)
                .IsRequired()
                .HasColumnName("payload")
                .HasColumnType("JSON");

            builder.Property(x => x.QueryParam)
                .IsRequired()
                .HasColumnName("queryParam")
                .HasColumnType("JSON");

            builder.Property(x => x.PathParam)
                .IsRequired()
                .HasColumnName("pathParam")
                .HasColumnType("JSON");
        }
    }
}
