﻿using AlfaDuo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AlfaDuo.Infra.Mappings
{
    public class SchedulerMap : IEntityTypeConfiguration<Scheduler>
    {
        public void Configure(EntityTypeBuilder<Scheduler> builder)
        {
            builder.ToTable("Scheduler");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.RuleId)
                .HasColumnType("BIGINT");

            builder.Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnName("description")
                .HasColumnType("VARCHAR(100)");

            builder.Property(x => x.Active)
                .IsRequired()
                .HasColumnName("active")
                .HasColumnType("bit");

            builder.Property(x => x.Cron)
                .IsRequired()
                .HasColumnName("cron")
                .HasColumnType("VARCHAR(20)");
        }
    }
}
