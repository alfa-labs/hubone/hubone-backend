﻿using AlfaDuo.Domain.Entities;

namespace AlfaDuo.Infra.Interfaces
{
    public interface ISchedulerRepository : IBaseRepository<Scheduler>
    {
    }
}
