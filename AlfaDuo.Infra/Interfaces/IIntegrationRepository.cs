﻿using AlfaDuo.Domain.Entities;

namespace AlfaDuo.Infra.Interfaces
{
    public interface IIntegrationRepository : IBaseRepository<Integration>
    {
    }
}
