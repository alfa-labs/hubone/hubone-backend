﻿using AlfaDuo.Domain.Entities;

namespace AlfaDuo.Infra.Interfaces
{
    public interface IRuleRepository : IBaseRepository<Rule>
    {
    }
}
