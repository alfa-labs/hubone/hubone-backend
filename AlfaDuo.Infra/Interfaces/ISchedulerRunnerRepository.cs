﻿using AlfaDuo.Domain.Entities;

namespace AlfaDuo.Infra.Interfaces
{
    public interface ISchedulerRunnerRepository : IBaseRepository<SchedulerRunner>
    {
    }
}
