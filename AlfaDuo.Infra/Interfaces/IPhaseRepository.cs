﻿using AlfaDuo.Domain.Entities;

namespace AlfaDuo.Infra.Interfaces
{
    public interface IPhaseRepository : IBaseRepository<Phase>
    {
    }
}
