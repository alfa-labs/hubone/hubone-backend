﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AlfaDuo.Infra.Context
{
    public class SchedulerRunnerContext : DbContext
    {
        public virtual DbSet<SchedulerRunner> SchedulerRunner { get; set; }

        public SchedulerRunnerContext()
        {
        }

        public SchedulerRunnerContext(DbContextOptions<SchedulerRunnerContext> options) : base(options)
        { }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder.EnableSensitiveDataLogging();
        */

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new SchedulerRunnerMap());
        }
    }
}
