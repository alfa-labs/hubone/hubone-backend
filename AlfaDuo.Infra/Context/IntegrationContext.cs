﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AlfaDuo.Infra.Context
{
    public class IntegrationContext : DbContext
    {
        public virtual DbSet<Integration> Integration { get; set; }

        public IntegrationContext()
        {
        }

        public IntegrationContext(DbContextOptions<IntegrationContext> options) : base(options)
        { }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder.EnableSensitiveDataLogging();
        */

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new IntegrationMap());
        }
    }
}
