﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AlfaDuo.Infra.Context
{
    public class PhaseContext : DbContext
    {
        public virtual DbSet<Phase> Phases { get; set; }

        public PhaseContext()
        {
        }

        public PhaseContext(DbContextOptions<PhaseContext> options) : base(options)
        { }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder.LogTo(Console.WriteLine);
        */

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PhaseMap());
        }
    }
}
