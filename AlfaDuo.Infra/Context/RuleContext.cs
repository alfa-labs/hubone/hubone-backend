﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AlfaDuo.Infra.Context
{
    public class RuleContext : DbContext
    {
        public virtual DbSet<Rule> Rules { get; set; }

        public RuleContext()
        {
        }

        public RuleContext(DbContextOptions<RuleContext> options) : base(options)
        { }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder.LogTo(Console.WriteLine);
        */

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new RuleMap());
        }
    }
}
