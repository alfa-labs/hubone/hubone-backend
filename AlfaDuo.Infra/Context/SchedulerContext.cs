﻿using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AlfaDuo.Infra.Context
{
    public class SchedulerContext : DbContext
    {
        public virtual DbSet<Scheduler> Schedulers { get; set; }

        public SchedulerContext()
        {
        }

        public SchedulerContext(DbContextOptions<SchedulerContext> options) : base(options)
        { }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder.LogTo(Console.WriteLine);
        */

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new SchedulerMap());
        }
    }
}
