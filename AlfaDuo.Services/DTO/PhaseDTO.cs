﻿namespace AlfaDuo.Services.DTO
{
    public class PhaseDTO
    {
        public long Id { get; set; }
        public long IntegrationId { get; private set; }
        public int Sequence { get; private set; }
        public string Rule { get; private set; }
        public string PhaseResult { get; private set; }

        public PhaseDTO(long id, long integrationId, int sequence, string rule, string phaseResult)
        {
            Id = id;
            IntegrationId = integrationId;
            Sequence = sequence;
            Rule = rule;
            PhaseResult = phaseResult;
        }

        public PhaseDTO() { }

    }
}
