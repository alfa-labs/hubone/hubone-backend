﻿namespace AlfaDuo.Services.DTO
{
    public class IntegrationDTO
    {
        public long Id { get; set; }
        public string Payload { get; set; }
        public string QueryParam { get; set; }
        public string PathParam { get; set; }
        public string Status { get; set; }
        public long IntegrationId { get; set; }

        public IntegrationDTO(long id, string payload, string queryParam, string pathParam, string status, long integrationId)
        {
            Id = id;
            Payload = payload;
            QueryParam = queryParam;
            PathParam = pathParam;
            Status = status;
            IntegrationId = integrationId;
        }

        public IntegrationDTO() { }
    }
}
