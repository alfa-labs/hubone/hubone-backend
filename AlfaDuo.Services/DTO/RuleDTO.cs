﻿namespace AlfaDuo.Services.DTO
{
    public class RuleDTO
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string? Endpoint { get; set; }
        public string? Verb { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
        public bool Synchronous { get; set; }
        public bool Active { get; set; }
        public string Rules { get; set; }

        public RuleDTO(long id, string description, string endpoint, string verb, string version, bool active, bool synchronous, string rules, string type)
        {
            Id = id;
            Description = description;
            Endpoint = endpoint;
            Verb = verb;
            Version = version;
            Active = active;
            Synchronous = synchronous;
            Rules = rules;
            Type = type;
        }

        public RuleDTO() { }

    }
}
