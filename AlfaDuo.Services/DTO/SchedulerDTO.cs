﻿namespace AlfaDuo.Services.DTO
{
    public class SchedulerDTO
    {
        public long Id { get; set; }
        public long RuleId { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public string Cron { get; set; }

        public SchedulerDTO(long id, long ruleId, string description, string cron, bool active)
        {
            RuleId = ruleId;
            Id = id;
            Description = description;
            Cron = cron;
            Active = active;
        }

        public SchedulerDTO() { }

    }
}
