﻿using AlfaDuo.Core.Structs;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Services.DTO;

namespace AlfaDuo.Services.Interfaces
{
    public interface IRuleService
    {
        Task<Optional<Rule>> CreateAsync(RuleDTO ruleDTO);
        Task<Optional<Rule>> UpdateAsync(RuleDTO ruleDTO, long id);
        Task RemoveAsync(long id);
        Task<Optional<Rule>> GetAsync(long id);
        Task<Optional<IList<Rule>>> GetAllAsync();

    }
}
