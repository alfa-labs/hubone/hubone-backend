﻿using AlfaDuo.Core.Structs;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Services.DTO;

namespace AlfaDuo.Services.Interfaces
{
    public interface ISchedulerService
    {
        Task<Optional<Scheduler>> CreateAsync(SchedulerDTO ruleDTO);
        Task<Optional<Scheduler>> UpdateAsync(SchedulerDTO ruleDTO, long id);
        Task RemoveAsync(long id);
        Task<Optional<Scheduler>> GetAsync(long id);
        Task<Optional<IList<Scheduler>>> GetAllAsync();

    }
}
