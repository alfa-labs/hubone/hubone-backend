﻿using AlfaDuo.Domain.Enum;
using Microsoft.AspNetCore.Http;
using System.Text.Json;

namespace AlfaDuo.Services.Interfaces
{
    public interface IIntegrationService
    {
        Task<dynamic> CreateAsync(
            HTTPOperation verb,
            string endpoint,
            dynamic queryString,
            PathString pathString,
            JsonElement? payload);
    }
}
