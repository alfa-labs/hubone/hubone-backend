﻿using AlfaDuo.Core.Communication.Mediator.Interfaces;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Core.Enum;
using AlfaDuo.Core.Structs;
using AlfaDuo.Core.Validations.Message;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Services.DTO;
using AlfaDuo.Services.Interfaces;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace AlfaDuo.Services.Services
{
    public class SchedulerService : ISchedulerService
    {
        private readonly ILogger<SchedulerService> _logger;
        private readonly IMapper _mapper;
        private readonly ISchedulerRepository _schedulerRepository;
        private readonly IRuleRepository _ruleRepository;
        private readonly IMediatorHandler _mediator;

        public SchedulerService(
            ILogger<SchedulerService> logger,
            IMediatorHandler mediator,
            ISchedulerRepository schedulerRepository,
            IRuleRepository ruleRepository,
            IMapper mapper)
        {
            _logger = logger;
            _mediator = mediator;
            _mapper = mapper;
            _schedulerRepository = schedulerRepository;
            _ruleRepository = ruleRepository;
        }

        public async Task<Optional<Scheduler>> CreateAsync(SchedulerDTO schedulerDTO)
        {
            try
            {
                Expression<Func<Scheduler, bool>> filter = scheduler
                   => scheduler.RuleId == schedulerDTO.RuleId
                   && schedulerDTO.Active;

                var ruleExists = await _findRule(schedulerDTO.RuleId);

                if (!ruleExists)
                {
                    _logger.LogWarning($"Regra: {schedulerDTO.RuleId} nao encontrada");

                    await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                        "Rule not found.",
                        DomainNotificationType.RecordNotFound));

                    return null;
                }

                var schedulerExists = await _schedulerRepository.GetAsync(filter);
                if (schedulerExists != null)
                {
                    _logger.LogWarning($"{ErrorMessages.RecordAlreadyExists}");

                    await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                        ErrorMessages.RecordAlreadyExists,
                        DomainNotificationType.RecordAlreadyExists));

                    return schedulerExists;
                }

                var scheduler = _mapper.Map<Scheduler>(schedulerDTO);
                var schedulerCreated = await _schedulerRepository.CreateAsync(scheduler);

                return schedulerCreated;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao invluir Scheduler", ex);
                throw;
            }
        }

        private async Task<bool> _findRule(long ruleId)
        {
            Expression<Func<Rule, bool>> filter = rule
           => rule.Id == ruleId
           && rule.Active;

            var rule = await _ruleRepository.GetAsync(filter);

            if (rule == null) return false;

            return true;
        }

        public async Task<Optional<Scheduler>> UpdateAsync(SchedulerDTO schedulerDTO, long id)
        {
            try
            {
                var schedulerExists = await _schedulerRepository.GetAsync(id);

                if (schedulerExists == null)
                {
                    await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                        ErrorMessages.RecordNotFound,
                        DomainNotificationType.RecordNotFound));
                }

                var scheduler = _mapper.Map<Scheduler>(schedulerDTO);
                scheduler.SetId(id);

                var schedulerCreated = await _schedulerRepository.UpdateAsync(scheduler);

                return schedulerCreated;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao atualizar o Scheduler", ex);
                throw;
            }
        }

        public async Task RemoveAsync(long id)
            => await _schedulerRepository.RemoveAsync(id);

        public async Task<Optional<Scheduler>> GetAsync(long id)
        {
            var scheduler = await _schedulerRepository.GetAsync(id);

            return _mapper.Map<Scheduler>(scheduler);
        }
        public async Task<Optional<IList<Scheduler>>> GetAllAsync()
        {
            try
            {
                var allSchedulers = await _schedulerRepository.GetAllAsync();
                var allSchedulersDTO = _mapper.Map<IList<Scheduler>>(allSchedulers);

                return new Optional<IList<Scheduler>>(allSchedulersDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao consultar listagem de registros", ex);
                throw;
            }
        }
    }
}
