﻿using AlfaDuo.Core.Communication.Mediator.Interfaces;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Core.Enum;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Domain.Enum;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Motor.Interfaces;
using AlfaDuo.Services.Interfaces;
using AlfaDuo.Shared.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;
using System.Text.Json;

namespace AlfaDuo.Services.Services
{
    public class IntegrationService : IIntegrationService
    {
        private readonly IMapper _mapper;
        private readonly IIntegrationRepository _integrationRepository;
        private readonly IRuleRepository _ruleRepository;
        private readonly IPhaseRepository _phaseRepository;
        private readonly IMediatorHandler _mediator;
        private readonly IMotorCore _motorCore;
        private readonly ILogger<IntegrationService> _logger;

        public IntegrationService(
            ILogger<IntegrationService> logger,
        IMediatorHandler mediator,
            IIntegrationRepository integrationRepository,
            IRuleRepository ruleRepository,
            IPhaseRepository phaseRepository,
            IMotorCore motorCore,
            IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
            _logger = logger;
            _integrationRepository = integrationRepository;
            _ruleRepository = ruleRepository;
            _phaseRepository = phaseRepository;

            _motorCore = motorCore;
        }

        public async Task<dynamic> CreateAsync(HTTPOperation verb,
            string endpoint,
            dynamic queryString,
            PathString pathParam,
            JsonElement? payload)
        {
            try
            {

                var pathParamList = pathParam.ToString().Split("/");
                var newPathList = new Dictionary<string, string>();

                int seqPath = 0;
                foreach (var param in pathParamList)
                {
                    if (!String.IsNullOrEmpty(param) && !param.Equals(endpoint))
                    {
                        newPathList.Add($"param{seqPath.ToString()}", param);
                        seqPath++;
                    }
                }

                var integration = new Integration
                {
                    Payload = JsonSerializer.Serialize(payload),
                    QueryParam = JsonSerializer.Serialize(queryString),
                    PathParam = JsonSerializer.Serialize(newPathList),
                    Status = IntegrationStatus.PENDING
                };

                Expression<Func<Rule, bool>> filter = rule
               => rule.Verb.ToLower() == verb.ToString().ToLower()
               && rule.Endpoint.ToLower() == endpoint.ToLower()
               && rule.Active;

                var rule = await _ruleRepository.GetAsync(filter);

                if (rule == null)
                {
                    _logger.LogWarning($"Regra/Endpoint nao encontrada");

                    await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   "Endpoint nao encontrado",
                   DomainNotificationType.RecordNotFound));

                    return null;
                }

                var integrationCreated = await _integrationRepository.CreateAsync(integration);

                await _generateRules(integrationCreated.Id, rule);

                if (rule.Synchronous)
                {
                    var result = await _motorCore.ProcessByIntegration(integrationCreated.Id);

                    _logger.LogInformation($"Resultado do Processamento {JsonSerializer.Serialize(result)}");
                    return result;
                }

                return integrationCreated;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro na criacao do registro de Integracao", ex);
                throw;
            }
        }

        private async Task<bool> _generateRules(long id, Rule layout)
        {
            var rules = JsonSerializer.Deserialize<List<RuleDetailDto>>(layout.Rules);

            foreach (var rule in rules)
            {
                var phase = new Phase();
                phase.setSequence(rule.Sequence);
                phase.setRule(JsonSerializer.Serialize(rule));
                phase.setType("1");
                phase.setIntegrationId(id);

                await _phaseRepository.CreateAsync(phase);
            }

            return false;
        }
    }
}
