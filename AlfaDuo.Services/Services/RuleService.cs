﻿using AlfaDuo.Core.Communication.Mediator.Interfaces;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Core.Enum;
using AlfaDuo.Core.Structs;
using AlfaDuo.Core.Validations.Message;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Services.DTO;
using AlfaDuo.Services.Interfaces;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;
using System.Text.Json;

namespace AlfaDuo.Services.Services
{
    public class RuleService : IRuleService
    {
        private readonly ILogger<RuleService> _logger;
        private readonly IMapper _mapper;
        private readonly IRuleRepository _ruleRepository;
        private readonly IMediatorHandler _mediator;

        public RuleService(
            ILogger<RuleService> logger,
            IMediatorHandler mediator,
            IRuleRepository ruleRepository,
            IMapper mapper)
        {
            _logger = logger;
            _mediator = mediator;
            _mapper = mapper;
            _ruleRepository = ruleRepository;
        }

        public async Task<Optional<Rule>> CreateAsync(RuleDTO ruleDTO)
        {
            try
            {
                Expression<Func<Rule, bool>> filter = rule
                   => rule.Verb.ToLower() == ruleDTO.Verb.ToLower()
                   && rule.Endpoint.ToLower() == ruleDTO.Endpoint.ToLower()
                   && rule.Active;

                var ruleExists = await _ruleRepository.GetAsync(filter);

                if (ruleExists != null)
                {
                    _logger.LogWarning($"{ErrorMessages.RecordAlreadyExists}: {JsonSerializer.Serialize(ruleExists)}");

                    await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                        ErrorMessages.RecordAlreadyExists,
                        DomainNotificationType.RecordAlreadyExists));

                    return ruleExists;
                }

                var rule = _mapper.Map<Rule>(ruleDTO);
                var ruleCreated = await _ruleRepository.CreateAsync(rule);

                return ruleCreated;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao incluir Regra", ex);
                throw;
            }
        }

        public async Task<Optional<Rule>> UpdateAsync(RuleDTO ruleDTO, long id)
        {
            try
            {
                var ruleExists = await _ruleRepository.GetAsync(id);

                if (ruleExists == null)
                {
                    await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                        ErrorMessages.RecordNotFound,
                        DomainNotificationType.RecordNotFound));
                }

                var rule = _mapper.Map<Rule>(ruleDTO);
                rule.SetId(id);

                var ruleCreated = await _ruleRepository.UpdateAsync(rule);

                return ruleCreated;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao atualizar regra", ex);
                throw;
            }
        }

        public async Task RemoveAsync(long id)
            => await _ruleRepository.RemoveAsync(id);

        public async Task<Optional<Rule>> GetAsync(long id)
        {
            try
            {
                var user = await _ruleRepository.GetAsync(id);

                return _mapper.Map<Rule>(user);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao consultar regra", ex);
                throw;
            }

        }
        public async Task<Optional<IList<Rule>>> GetAllAsync()
        {
            try
            {
                var allUsers = await _ruleRepository.GetAllAsync();
                var allUsersDTO = _mapper.Map<IList<Rule>>(allUsers);

                return new Optional<IList<Rule>>(allUsersDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao consultar regra", ex);
                throw;
            }
        }
    }
}
