﻿namespace AlfaDuo.Core.Enum
{
    public enum DomainNotificationType
    {
        RecordAlreadyExists,
        RecordInvalid,
        RecordNotFound,
    }
}
