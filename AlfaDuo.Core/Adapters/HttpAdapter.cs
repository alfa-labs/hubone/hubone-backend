﻿using AlfaDuo.Core.Interfaces;
using Microsoft.Extensions.Logging;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace AlfaDuo.Core.Adapters
{
    public class HttpAdapter : IHttpAdapter
    {
        private ILogger<HttpAdapter> _logger;

        public HttpAdapter(ILogger<HttpAdapter> logger)
        {
            _logger = logger;
        }

        public async Task<T> Call<T>(
            HttpMethod method,
            string uri,
            string endPoint,
            Dictionary<string, string> headers,
            object? obj = null) where T : class
        {
            _logger.LogInformation($"Inicializando chamada HTTP: {method} - {endPoint} - {uri} - JSON: {JsonSerializer.Serialize(obj)}");

            try
            {
                var baseUrl = new Uri($"{uri}");
                var client = new HttpClient();

                client.BaseAddress = baseUrl;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                foreach(var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
                
                JsonSerializerOptions options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                using (var httpRequestMensage = new HttpRequestMessage(method, endPoint))
                {
                    if (obj != null)
                    {
                        var json = JsonSerializer.Serialize(obj);
                        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                        httpRequestMensage.Content = stringContent;
                    }

                    var response = await client.SendAsync(httpRequestMensage);
                    var content = await response.Content.ReadAsStringAsync();

                    _logger.LogInformation($"Retorno da chamada HTTP: {method} - {endPoint} - {uri} - StatusCode: {response.StatusCode} Conteudo: {content}");

                    var contentToObject = JsonSerializer.Deserialize<T>(content);

                    return contentToObject;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao realizar chamada HTTP", ex);
                throw;
            }

        }
    }
}
