﻿using AlfaDuo.Core.Interfaces;
using AlfaDuo.Domain.Configuration;
using Dapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sap.Data.Hana;

namespace AlfaDuo.Core.Adapters
{
    public class HanaAdapter : IHanaAdapter
    {
        private readonly string _urlConnection;
        private readonly ILogger<HanaAdapter> _logger;

        public HanaAdapter(IOptions<Configuration> configurations, ILogger<HanaAdapter> logger)
        {
            _logger = logger;
            HanaDbConnection cfgFile = configurations.Value.HanaDbConnection;
            _urlConnection = $"Server={cfgFile.Server};UserID={cfgFile.UserID};Password={cfgFile.Password};CS={cfgFile.Database}";
        }

        public async Task<T> QueryFirst<T>(string sql)
        {
            _logger.LogInformation($"Realizando consulta : {sql}");

            try
            {
                using (var _connection = new HanaConnection(_urlConnection))
                {

                    var result = await _connection.QueryFirstOrDefaultAsync<T>(sql);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao realizar a consulta: {sql}", ex);
                throw;
            }
        }
        public async Task<IEnumerable<T>> Query<T>(string sql)
        {
            _logger.LogInformation($"Realizando consulta : {sql}");

            try
            {
                using (var _connection = new HanaConnection(_urlConnection))
                {
                    var result = await _connection.QueryAsync<T>(sql);
                    return result;
                }
            }
            catch (HanaException ex)
            {
                _logger.LogError($"Erro ao realizar a consulta: {sql}", ex);
                throw;
            }
        }

        public async Task<int> Execute(string sql)
        {
            _logger.LogInformation($"Realizando Execucao : {sql}");

            try
            {
                using (var _connection = new HanaConnection(_urlConnection))
                {

                    var result = await _connection.ExecuteAsync(sql);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao realizar a Execucao: {sql}", ex);
                throw;
            }
        }
    }
}
