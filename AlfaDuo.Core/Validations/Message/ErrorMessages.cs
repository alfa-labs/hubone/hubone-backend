﻿namespace AlfaDuo.Core.Validations.Message
{
    public static class ErrorMessages
    {
        public const string RecordAlreadyExists
            = "Ja existe um registro cadastrado com essas caracteristicas.";

        public const string RecordNotFound
            = "Nao existe nenhum registro com o id informado.";

        public static string RecordInvalid(string errors)
            => "Os campos informados para o Registro estao invalidos" + errors;
    }
}
