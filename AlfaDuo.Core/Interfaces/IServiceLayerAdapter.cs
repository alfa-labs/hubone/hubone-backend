﻿using System.Net;

namespace AlfaDuo.Core.Interfaces
{
    public interface IServiceLayerAdapter
    {
        Task<T> Call<T>(string endPoint, HttpMethod method, string uri, object? obj = null, string? sessionId = null) where T : class;
        Task<CookieContainer> Login();
    }
}