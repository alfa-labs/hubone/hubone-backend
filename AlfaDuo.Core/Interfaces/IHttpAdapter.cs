﻿namespace AlfaDuo.Core.Interfaces
{
    public interface IHttpAdapter
    {
        Task<T> Call<T>(
            HttpMethod method,
            string endPoint,
            string uri,
            Dictionary<string, string> headers,
            object obj = null) where T : class;
    }
}
