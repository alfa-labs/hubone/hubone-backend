﻿using AlfaDuo.Core.Communication.Messages.Notifications;

namespace AlfaDuo.Core.Communication.Mediator.Interfaces
{
    public interface IMediatorHandler
    {
        Task PublishDomainNotificationAsync<T>(T appNotification)
            where T : DomainNotification;
    }
}
