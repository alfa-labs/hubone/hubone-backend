﻿namespace AlfaDuo.Domain.Enum
{
    public enum IntegrationStatus
    {
        PENDING = 0,
        PROCESSING = 1,
        FINISHED = 2,
        ERROR = 3,
    }
}
