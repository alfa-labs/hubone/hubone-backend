﻿namespace AlfaDuo.Domain.Configuration
{
    public class Configuration
    {
        public ServiceLayer? ServiceLayer { get; set; }
        public HanaDbConnection? HanaDbConnection { get; set; }
        public Cache? Cache { get; set; }
    }

    public class HanaDbConnection
    {
        public string Server { get; set; } = "";
        public string UserID { get; set; } = "";
        public string Password { get; set; } = "";
        public string Database { get; set; } = "";
    }

    public class ServiceLayer
    {
        public string SessionId { get; set; } = "";
        public string Uri { get; set; } = "";
        public string CompanyDB { get; set; } = "";
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public int Language { get; set; }
    }

    public class Cache
    {
        public AuthConfig Auth { get; set; }
    }

    public class AuthConfig
    {
        public string SchemaName { get; set; } = "";
        public string TableName { get; set; } = "";
        public string ConnectionString { get; set; } = "";

    }
}
