﻿namespace AlfaDuo.Domain.Entities
{
    public class Rule : Base
    {
        public string? Endpoint { get; private set; }
        public string? Verb { get; private set; }
        public string Type { get; private set; }
        public string Description { get; private set; }
        public string Version { get; private set; }
        public bool Synchronous { get; private set; }
        public bool Active { get; private set; }
        public string Rules { get; private set; }

        protected Rule() { }

        public Rule(string endpoint, string verb, string version, bool synchronous, bool active, string rules, string description, string type, long id)
        {
            Endpoint = endpoint;
            Verb = verb;
            Version = version;
            Synchronous = synchronous;
            Active = active;
            Rules = rules;
            Type = type;
            Description = description;
            Id = id;

            _errors = new List<string>();
        }

        public void SetEndpoint(string endpoint)
        {
            Endpoint = endpoint;
        }
        public void SetVerb(string verb)
        {
            Verb = verb;
        }
        public void SetSynchronous(bool synchronous)
        {
            Synchronous = synchronous;
        }
        public void SetRules(string rules)
        {
            Rules = rules;
        }
        public void SetId(long id)
        {
            Id = id;
        }

        public void SetVersion(string version)
        {
            Version = version;
        }

        public void SetType(string type)
        {
            Type = type;
        }

        public void SetDescription(string description)
        {
            Description = description;
        }

        public void SetActive(bool active)
        {
            Active = active;
        }
    }
}
