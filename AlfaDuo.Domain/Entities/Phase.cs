﻿
namespace AlfaDuo.Domain.Entities
{
    public class Phase : Base
    {
        public long IntegrationId { get; private set; }
        public int Sequence { get; private set; }
        public string Rule { get; private set; }
        public string PhaseResult { get; private set; }
        public string Type { get; private set; }

        public Phase() { }

        public Phase(long integrationId, int sequence, string rule, string phaseResult, string type)
        {
            IntegrationId = integrationId;
            Sequence = sequence;
            Rule = rule;
            PhaseResult = phaseResult;
            Type = type;
        }

        public void setIntegrationId(long integrationId)
        {
            IntegrationId = integrationId;
        }

        public void setSequence(int sequence)
        {
            Sequence = sequence;
        }

        public void setRule(string rule)
        {
            Rule = rule;
        }

        public void setPhaseResult(string phaseResult)
        {
            PhaseResult = phaseResult;
        }

        public void setType(string type)
        {
            Type = type;
        }
    }
}
