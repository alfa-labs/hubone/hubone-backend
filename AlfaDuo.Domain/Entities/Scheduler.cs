﻿namespace AlfaDuo.Domain.Entities
{
    public class Scheduler : Base
    {
        public string Description { get; private set; } = "";
        public long RuleId { get; private set; }
        public string Cron { get; private set; } = "";
        public bool Active { get; private set; }

        protected Scheduler() { }

        public Scheduler(long id, string description, long ruleId, string cron, bool active)
        {
            Description = description;
            RuleId = ruleId;
            Cron = cron;
            Active = active;
            Id = id;

            _errors = new List<string>();
        }

        public void SetId(long id) { Id = id; }
        public void SetDescription(string description) { Description = description; }
        public void SetRuleId(long ruleId) { RuleId = ruleId; }
        public void SetCron(string cron) { Cron = cron; }
        public void SetActive(bool active) { Active = active; }
    }
}
