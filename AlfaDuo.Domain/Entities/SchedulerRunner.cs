﻿namespace AlfaDuo.Domain.Entities
{
    public class SchedulerRunner : Base
    {
        public long SchedulerId { get; set; }
        public DateTime StartAt { get; set; }

        public SchedulerRunner() { }

        public SchedulerRunner(long id, long schedulerId, DateTime startAt)
        {
            SchedulerId = schedulerId;
            StartAt = startAt;
            Id = id;

            _errors = new List<string>();
        }

        public void SetScheduleId(long schedulerId)
        {
            SchedulerId = schedulerId;
        }

        public void SetStartAt(DateTime startAt)
        {
            StartAt = startAt;
        }
    }
}
