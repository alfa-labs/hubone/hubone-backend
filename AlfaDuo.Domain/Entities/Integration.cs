﻿using AlfaDuo.Domain.Enum;

namespace AlfaDuo.Domain.Entities
{
    public class Integration : Base
    {
        public string Payload { get; set; }
        public string QueryParam { get; set; }
        public string PathParam { get; set; }
        public IntegrationStatus Status { get; set; }
        public long RuleId { get; set; }

        public Integration() { }

        public Integration(string payload, string queryParam, string pathParam, IntegrationStatus status, long ruleId)
        {
            Payload = payload;
            QueryParam = queryParam;
            PathParam = pathParam;
            Status = status;
            RuleId = ruleId;
        }

        public void SetPayload(string payload)
        {
            Payload = payload;
        }

        public void SetQueryParam(string queryParam)
        {
            QueryParam = queryParam;
        }

        public void SetPathParam(string pathParam)
        {
            PathParam = pathParam;
        }

        public void SetStatus(IntegrationStatus status)
        {
            Status = status;
        }

        public void SetRuleId(long ruleId)
        {
            RuleId = ruleId;
        }
    }
}
