﻿namespace AlfaDuo.Shared.Dto
{
    public class PhaseDto
    {
        public long Id { get; set; }
        public long IntegrationId { get; set; }
        public int Sequence { get; set; }
        public RuleDetailDto Rule { get; set; }
        public object PhaseResult { get; set; }

        public PhaseDto(long id, long integrationId, int sequence, RuleDetailDto rule, string phaseResult)
        {
            Id = id;
            IntegrationId = integrationId;
            Sequence = sequence;
            Rule = rule;
            PhaseResult = phaseResult;
        }

        public PhaseDto() { }

    }
}
