﻿namespace AlfaDuo.Shared.Dto
{
    public class LoginServiceLayerDto
    {
        public string SessionId { get; set; }
        public string Version { get; set; }
        public int SessionTimeout { get; set; }

        public LoginServiceLayerDto()
        {
        }
    }
}
