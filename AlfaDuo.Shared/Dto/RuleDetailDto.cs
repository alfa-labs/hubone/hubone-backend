﻿using System.Text.Json.Serialization;

namespace AlfaDuo.Shared.Dto
{
    public class RuleDetailDto
    {
        [JsonPropertyName("type")]
        public string Type { get; set; } = string.Empty;

        [JsonPropertyName("query")]
        public QueryDetail? Query { get; set; }

        [JsonPropertyName("serviceLayer")]
        public ServiceLayerDetail? ServiceLayer { get; set; }

        [JsonPropertyName("rest")]
        public RestDetail? Rest { get; set; }

        [JsonPropertyName("sequence")]
        public int Sequence { get; set; }

        [JsonPropertyName("nextSequence")]
        public NextSequenceDetail? NextSequence { get; set; }

        [JsonPropertyName("resultFormat")]
        public List<ResultFormatDetail>? ResultFormat { get; set; }

        [JsonPropertyName("conditionToSuccess")]
        public string ConditionToSuccess { get; set; } = string.Empty;
    }

    public class QueryDetail
    {
        [JsonPropertyName("sql")]
        public string Sql { get; set; } = string.Empty;

        [JsonPropertyName("execute")]
        public bool Execute { get; set; }

        [JsonPropertyName("onlyOne")]
        public bool OnlyOne { get; set; }

        [JsonPropertyName("parameters")]
        public List<QueryParameters>? Parameters { get; set; }
    }

    public class QueryParameters
    {
        [JsonPropertyName("origin")]
        public int Origin { get; set; }

        [JsonPropertyName("order")]
        public int Order { get; set; }

        [JsonPropertyName("value")]
        public string Value { get; set; } = string.Empty;
    }

    public class ParametersDetail
    {
        [JsonPropertyName("origin")]
        public int Origin { get; set; }

        [JsonPropertyName("field")]
        public string Field { get; set; } = string.Empty;

        [JsonPropertyName("value")]
        public dynamic? Value { get; set; }
    }

    public class ParametersPathDetail
    {
        [JsonPropertyName("origin")]
        public int Origin { get; set; }

        [JsonPropertyName("order")]
        public int Order { get; set; }

        [JsonPropertyName("value")]
        public dynamic? Value { get; set; }
    }

    public class NextSequenceDetail
    {
        [JsonPropertyName("sequenceIfOk")]
        public int SequenceIfOk { get; set; }

        [JsonPropertyName("sequenceIfNotOk")]
        public int SequenceIfNotOk { get; set; }
    }

    public class ServiceLayerDetail
    {
        [JsonPropertyName("verb")]
        public string Verb { get; set; } = string.Empty;

        [JsonPropertyName("endpoint")]
        public string Endpoint { get; set; } = string.Empty;

        [JsonPropertyName("bodyParameters")]
        public BodyParameterDetail BodyParameter { get; set; }
        
        [JsonPropertyName("pathParameters")]
        public List<ParametersPathDetail>? PathParameters { get; set; }

        [JsonPropertyName("queryParameters")]
        public List<ParametersDetail>? QueryParameters { get; set; }

        [JsonPropertyName("entityKey")]
        public EntityKeyDetail? EntityKey { get; set; }
    }

    public class RestDetail
    {
        [JsonPropertyName("auth")]
        public AuthDetail Auth { get; set; }

        [JsonPropertyName("verb")]
        public string Verb { get; set; } = string.Empty;

        [JsonPropertyName("baseUrl")]
        public string BaseUrl { get; set; } = string.Empty;

        [JsonPropertyName("endpoint")]
        public string Endpoint { get; set; } = string.Empty;

        [JsonPropertyName("headers")]
        public List<HeaderDetail>? Headers { get; set; }

        [JsonPropertyName("bodyParameters")]
        public BodyParameterDetail BodyParameter { get; set; }

        [JsonPropertyName("pathParameters")]
        public List<ParametersPathDetail>? PathParameters { get; set; }

        [JsonPropertyName("queryParameters")]
        public List<ParametersDetail>? QueryParameters { get; set; }
    }

    public class ResultFormatDetail
    {
        [JsonPropertyName("field")]
        public string Field { get; set; } = string.Empty;

        [JsonPropertyName("value")]
        public dynamic? Value { get; set; }
    }

    public class EntityKeyDetail
    {
        [JsonPropertyName("origin")]
        public int Origin { get; set; }

        [JsonPropertyName("value")]
        public string Value { get; set; } = string.Empty;
    }

    public class AuthDetail
    {
        [JsonPropertyName("verb")]
        public string Verb { get; set; } = string.Empty;

        [JsonPropertyName("internalAuthKey")]
        public string InternalAuthKey { get; set; } = string.Empty;

        [JsonPropertyName("type")]
        public string Type { get; set; } = string.Empty;

        [JsonPropertyName("baseUrl")]
        public string BaseUrl { get; set; } = string.Empty;

        [JsonPropertyName("endpoint")]
        public string EndPoint { get; set; } = string.Empty;

        [JsonPropertyName("headers")]
        public List<HeaderDetail>? Headers { get; set; }

        [JsonPropertyName("bodyParameters")]
        public List<AuthDetailValues>? BodyParameters { get; set; }

        [JsonPropertyName("queryParameters")]
        public List<AuthDetailValues>? QueryParameters { get; set; }
    }

    public class AuthDetailValues
    {
        [JsonPropertyName("field")]
        public string Field { get; set; } = string.Empty;

        [JsonPropertyName("value")]
        public dynamic? Value { get; set; }
    }

    public class HeaderDetail
    {
        [JsonPropertyName("key")]
        public string Key { get; set; } = string.Empty;

        [JsonPropertyName("value")]
        public string Value { get; set; } = string.Empty;
    }

    public class BodyParameterDetail
    {
        [JsonPropertyName("iterate")]
        public bool Iterate { get; set; }

        [JsonPropertyName("originIterate")]
        public int OriginIterate { get; set; }

        [JsonPropertyName("fields")]
        public List<ParametersDetail>? Fields { get; set; }
    }
}
