﻿using System.ComponentModel.DataAnnotations;

namespace AlfaDuo.API.ViewModels
{
    public class SchedulerViewModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "O atributo Description deve ser informado.")]
        public string Description { get; set; } = "";

        [Required(ErrorMessage = "O atributo RuleId deve ser informado.")]
        public long RuleId { get; set; }

        [Required(ErrorMessage = "O atributo Cron deve ser informado.")]
        public string Cron { get; set; } = "";

        [Required(ErrorMessage = "O atributo Active deve ser informado.")]
        public bool Active { get; set; }
    }
}
