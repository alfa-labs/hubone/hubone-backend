﻿using System.ComponentModel.DataAnnotations;

namespace AlfaDuo.API.ViewModels
{
    public class RuleViewModel
    {
        public long Id { get; set; }
        public string Description { get; set; } = "";
        public string Endpoint { get; set; } = "";
        public string Verb { get; set; } = "";
        public string Type { get; set; } = "";

        [Required(ErrorMessage = "O atributo Version deve ser informado.")]
        public string Version { get; set; } = "";

        [Required(ErrorMessage = "O atributo Active deve ser informado.")]
        public bool Active { get; set; }

        [Required(ErrorMessage = "O atributo Synchronous deve ser informado.")]
        public bool Synchronous { get; set; }

        [Required(ErrorMessage = "O atributo Rules deve ser informado.")]
        public dynamic Rules { get; set; } = "";
    }
}
