using AlfaDuo.API.ViewModels;
using AlfaDuo.Core.Adapters;
using AlfaDuo.Core.Communication.Handlers;
using AlfaDuo.Core.Communication.Mediator;
using AlfaDuo.Core.Communication.Mediator.Interfaces;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Core.Interfaces;
using AlfaDuo.Domain.Configuration;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Infra.Context;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Infra.Repositories;
using AlfaDuo.Motor.Core;
using AlfaDuo.Motor.Interfaces;
using AlfaDuo.Services.DTO;
using AlfaDuo.Services.Interfaces;
using AlfaDuo.Services.Services;
using AlfaDuo.Shared.Dto;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NLog;
using NLog.Web;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

var CONN_STRING = builder.Configuration.GetConnectionString("DefaultConnection");

GlobalDiagnosticsContext.Set("configDir", "\\Logs");
GlobalDiagnosticsContext.Set("connectionString", CONN_STRING);
var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

try
{
    // Add services to the container.
    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "AlfaDUO",
            Version = "v1",
            Description = "ESB para integra��o entre o ERP SAP Business One e outras plataformas.",
            Contact = new OpenApiContact
            {
                Name = "Alfa Tecnologia e Inova��o",
                Email = "contato@alfaerp.com.br",
                Url = new Uri("https://www.alfaerp.com.br")
            },
        });
    });

    #region AutoMapper

    var autoMapperConfig = new MapperConfiguration(cfg =>
    {
        cfg.CreateMap<RuleDTO, Rule>().ReverseMap();
        cfg.CreateMap<Rule, RuleDTO>().ReverseMap();
        cfg.CreateMap<RuleViewModel, RuleDTO>().ReverseMap();
        cfg.CreateMap<SchedulerViewModel, SchedulerDTO>().ReverseMap();
        cfg.CreateMap<Scheduler, SchedulerDTO>().ReverseMap();
        cfg.CreateMap<SchedulerDTO, Scheduler>().ReverseMap();
        cfg.CreateMap<PhaseDto, Phase>().ReverseMap();
        cfg.CreateMap<Phase, PhaseDto>().ReverseMap();
    });

    builder.Services.AddSingleton(autoMapperConfig.CreateMapper());

    #endregion

    #region DataBase
    builder.Services.AddDbContext<RuleContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<IntegrationContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<PhaseContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<SchedulerContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    builder.Services.AddDbContext<SchedulerRunnerContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
    #endregion

    #region Repositories
    builder.Services.AddScoped<IRuleRepository, RuleRepository>();
    builder.Services.AddScoped<IIntegrationRepository, IntegrationRepository>();
    builder.Services.AddScoped<IPhaseRepository, PhaseRepository>();
    builder.Services.AddScoped<ISchedulerRepository, SchedulerRepository>();
    #endregion

    #region Services
    builder.Services.AddScoped<IRuleService, RuleService>();
    builder.Services.AddScoped<IIntegrationService, IntegrationService>();
    builder.Services.AddScoped<IHttpAdapter, HttpAdapter>();
    builder.Services.AddScoped<IHanaAdapter, HanaAdapter>();
    builder.Services.AddScoped<IServiceLayerAdapter, ServiceLayerAdapter>();
    builder.Services.AddScoped<IIntegrationService, IntegrationService>();
    builder.Services.AddScoped<ISchedulerService, SchedulerService>();

    builder.Services.AddScoped<IMotorCore, MotorCore>();
    #endregion

    #region Mediator

    builder.Services.AddMediatR(typeof(Program));
    builder.Services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
    builder.Services.AddScoped<IMediatorHandler, MediatorHandler>();

    #endregion

    #region Configure
    builder.Services.Configure<Configuration>(configuration.GetSection("AppConfig"));
    #endregion

    #region NLog
    builder.Logging.ClearProviders();
    builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
    builder.Host.UseNLog();
    #endregion

    #region Cache
    builder.Services.AddDistributedMySqlCache(options =>
    {
        options.ConnectionString = CONN_STRING;
        options.SchemaName = "alfa_duo";
        options.TableName = "api_auth_cache";
    });
    #endregion

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception exception)
{
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    NLog.LogManager.Shutdown();
}