﻿using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Core.Util;
using AlfaDuo.Domain.Enum;
using AlfaDuo.Services.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace AlfaDuo.API.Controllers
{
    [Route("/")]
    [ApiController]
    public class IntegrationController : BaseController
    {
        private const string ANY_PAR = "{any0?}/{any1?}/{any2?}/{any3?}/{any4?}/{any5?}/{any6?}/{any7?}/{any8?}/{any9?}/{any10?}/{any11?}/{any12?}/{any13?}/{any14?}/";

        private readonly ILogger<IntegrationController> _logger;
        private readonly IMapper _mapper;
        private readonly IIntegrationService _integrationService;

        public IntegrationController(
            ILogger<IntegrationController> logger,
            IMapper mapper,
            IIntegrationService integrationService,
            INotificationHandler<DomainNotification> domainNotificationHandler)
            : base(domainNotificationHandler)
        {
            _mapper = mapper;
            _integrationService = integrationService;
            _logger = logger;
        }

        [HttpPost("{endPoint}/" + ANY_PAR)]
        public async Task<ActionResult> CreatePostAsync(
            string endpoint,
            [ModelBinder(BinderType = typeof(DynamicQueryBinder))] dynamic queries,
            [FromBody] JsonElement payload)
        {
            var pathParam = Request.Path;

            var result = await _integrationService.CreateAsync(
                HTTPOperation.POST,
                endpoint,
                queries,
                pathParam,
                payload);

            if (HasNotifications())
            {
                _logger.LogWarning($"Inconsistencia na chamada do endpoint {endpoint}");
                return Result();
            }

            _logger.LogInformation($"Finalizada a execucao do endpoint: {endpoint} - resultado: {JsonSerializer.Serialize(result)}");

            return Ok(result);
        }

        [HttpGet("{endPoint}/" + ANY_PAR)]
        public async Task<ActionResult> CreateGetAsync(
            string endpoint,
            [ModelBinder(BinderType = typeof(DynamicQueryBinder))] dynamic queries)
        {
            var pathParam = Request.Path;

            _logger.LogInformation($"Iniciando a execucao do endpoint: {endpoint} - pathParam {pathParam} - querystring: {queries}");

            if (!pathParam.ToString().Contains("/swagger"))
            {
                var result = await _integrationService.CreateAsync(
                    HTTPOperation.GET,
                    endpoint,
                    queries,
                    pathParam,
                    null);

                _logger.LogInformation($"Finalizacao da chamada do endpoint {endpoint} - JSON {JsonSerializer.Serialize(result)}");

                return Ok(result);
            }

            return Ok();
        }

        [HttpPut("{endPoint}/" + ANY_PAR)]
        public async Task<ActionResult> CreatePutAsync(
            string endpoint,
            [ModelBinder(BinderType = typeof(DynamicQueryBinder))] dynamic queries,
            [FromBody] JsonElement payload)
        {
            var pathParam = Request.Path;

            _logger.LogInformation($"Iniciando a execucao do endpoint: {endpoint} - pathParam: {pathParam} - querystring: {queries} - payload: {JsonSerializer.Serialize(payload)}");

            var result = await _integrationService.CreateAsync(
                HTTPOperation.PUT,
                endpoint,
                queries,
                pathParam,
                null);

            _logger.LogInformation($"Fim da execucao do endpoint: {endpoint} - JSON: {JsonSerializer.Serialize(result)}");

            return Ok(result);
        }

        [HttpPatch("{endPoint}/" + ANY_PAR)]
        public async Task<ActionResult> CreatePatchAsync(
            string endpoint,
            [ModelBinder(BinderType = typeof(DynamicQueryBinder))] dynamic queries,
            [FromBody] JsonElement payload)
        {
            var pathParam = Request.Path;

            _logger.LogInformation($"Iniciando a execucao do endpoint: {endpoint} - pathParam: {pathParam} - querystring: {queries} - payload: {JsonSerializer.Serialize(payload)}");

            var result = await _integrationService.CreateAsync(
                HTTPOperation.PATCH,
                endpoint,
                queries,
                pathParam,
                null);

            _logger.LogInformation($"Fim da execucao do endpoint JSON: {JsonSerializer.Serialize(result)}");

            return Ok(result);
        }

        [HttpDelete("{endPoint}/" + ANY_PAR)]
        public async Task<ActionResult> CreateDeleteAsync(
            string endpoint,
            [ModelBinder(BinderType = typeof(DynamicQueryBinder))] dynamic queries)
        {
            var pathParam = Request.Path;

            _logger.LogInformation($"Iniciando a execucao do endpoint: {endpoint} - pathParam: {pathParam} - querystring: {queries}");

            var result = await _integrationService.CreateAsync(
                HTTPOperation.DELETE,
                endpoint,
                queries,
                pathParam,
                null);

            _logger.LogInformation($"Fim da execucao do endpoint: {endpoint} - pathParam: {pathParam} - querystring: {queries} - JSON: {JsonSerializer.Serialize(result)}");

            return Ok(result);
        }
    }
}
