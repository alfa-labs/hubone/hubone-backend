﻿using AlfaDuo.API.ViewModels;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Services.DTO;
using AlfaDuo.Services.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace AlfaDuo.API.Controllers
{
    [Route("api/v1/scheduler")]
    [ApiController]
    public class SchedulerController : BaseController
    {
        private readonly ILogger<SchedulerController> _logger;
        private readonly IMapper _mapper;
        private readonly ISchedulerService _schedulerService;

        public SchedulerController(
            IMapper mapper,
            ISchedulerService schedulerService,
            ILogger<SchedulerController> logger,
            INotificationHandler<DomainNotification> domainNotificationHandler)
            : base(domainNotificationHandler)
        {
            _logger = logger;
            _mapper = mapper;
            _schedulerService = schedulerService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] SchedulerViewModel schedulerViewModel)
        {
            _logger.LogInformation($"Iniciando a inclusao de um novo agendamento JSON: {JsonSerializer.Serialize(schedulerViewModel)}");

            var schedulerDTO = _mapper.Map<SchedulerDTO>(schedulerViewModel);
            var result = await _schedulerService.CreateAsync(schedulerDTO);

            if (HasNotifications())
            {
                _logger.LogWarning($"Houve um erro a inclusao de uma nova regra");
                return Result();
            }

            _logger.LogInformation($"Agendamento incluido com sucesso JSON: {JsonSerializer.Serialize(result)}");

            return Ok(new ResultViewModel
            {
                Message = "Registro inserido com sucesso!",
                Success = true,
                Data = result
            });
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateAsync([FromBody] SchedulerViewModel schedulerViewModel, long id)
        {
            _logger.LogInformation($"Iniciando a atualizacao do agendamento {id} - JSON: {schedulerViewModel}");

            var schedulerDTO = _mapper.Map<SchedulerDTO>(schedulerViewModel);
            var result = await _schedulerService.UpdateAsync(schedulerDTO, id);

            if (HasNotifications())
            {
                _logger.LogWarning($"Inconsistência na atualizacao do agendamento {id}");
                return Result();
            }

            _logger.LogInformation($"Agendamento atualizado com sucesso {id} - JSON: {result}");

            return Ok(new ResultViewModel
            {
                Message = "Registro atualizado com sucesso!",
                Success = true,
                Data = result
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveAsync(long id)
        {
            _logger.LogInformation($"Iniciando a exclusao do agendamento {id}");

            await _schedulerService.RemoveAsync(id);

            if (HasNotifications())
            {
                _logger.LogWarning($"Inconsistencia na exclusao do agendamento {id}");
                return Result();
            }

            _logger.LogInformation($"Agendamento excluido com sucesso {id}");

            return Ok(new ResultViewModel
            {
                Message = "Registro removido com sucesso!",
                Success = true,
                Data = null
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(long id)
        {
            _logger.LogInformation($"Iniciando a consulta do agendamento {id}");

            var scheduler = await _schedulerService.GetAsync(id);

            if (HasNotifications())
            {
                _logger.LogWarning($"Inconsistência na consulta do agendamento {id}");
                return Result();
            }

            if (scheduler.Value == null)
            {
                _logger.LogWarning($"Nenhum Agendamento foi encontrado com o ID: {id}");

                return Ok(new ResultViewModel
                {
                    Message = "Nenhum Scheduler foi encontrado com o ID informado.",
                    Success = true,
                    Data = null
                });
            }

            _logger.LogInformation($"Agendamento foi encontrado com o ID: {id}  - JSON: {scheduler.Value}");

            return Ok(new ResultViewModel
            {
                Message = "Schedulers encontrado com sucesso!",
                Success = true,
                Data = scheduler.Value
            });
        }

        [HttpGet]
        [Route("get-all")]
        public async Task<IActionResult> GetAsync()
        {
            _logger.LogInformation($"Iniciando a consulta da listagem de agendamentos");

            var allSchedulers = await _schedulerService.GetAllAsync();

            if (HasNotifications())
            {
                _logger.LogWarning($"Inconsistência na consulta da listagem de agendamentos");
                return Result();
            }

            _logger.LogInformation($"Agendamentos encontrados com sucesso  - JSON: {allSchedulers.Value}");

            return Ok(new ResultViewModel
            {
                Message = "Schedulers encontrados com sucesso!",
                Success = true,
                Data = allSchedulers.Value
            });
        }

    }

}
