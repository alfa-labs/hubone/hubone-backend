﻿using AlfaDuo.API.ViewModels;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Services.DTO;
using AlfaDuo.Services.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace AlfaDuo.API.Controllers
{
    [Route("api/v1/rule")]
    [ApiController]
    public class RuleController : BaseController
    {
        private readonly ILogger<RuleController> _logger;
        private readonly IMapper _mapper;
        private readonly IRuleService _ruleService;

        public RuleController(
            ILogger<RuleController> logger,
            IMapper mapper,
            IRuleService ruleService,
            INotificationHandler<DomainNotification> domainNotificationHandler)
            : base(domainNotificationHandler)
        {
            _logger = logger;
            _mapper = mapper;
            _ruleService = ruleService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] RuleViewModel ruleViewModel)
        {
            _logger.LogInformation($"Iniciando a inclusao de uma nova regra - JSON: {JsonSerializer.Serialize(ruleViewModel)}");

            var ruleDTO = _mapper.Map<RuleDTO>(ruleViewModel);

            var result = await _ruleService.CreateAsync(ruleDTO);

            if (HasNotifications())
            {
                _logger.LogWarning($"Houve um erro na inclusao de regras");
                return Result();
            }

            _logger.LogInformation($"Regra incluida com sucesso - JSON: {JsonSerializer.Serialize(result)}");

            return Ok(new ResultViewModel
            {
                Message = "Registro inserido com sucesso!",
                Success = true,
                Data = result
            });
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateAsync([FromBody] RuleViewModel ruleViewModel, long id)
        {
            _logger.LogInformation($"Iniciando a atualizacao da regra: {id} - JSON: {JsonSerializer.Serialize(ruleViewModel)}");

            var ruleDTO = _mapper.Map<RuleDTO>(ruleViewModel);
            var result = await _ruleService.UpdateAsync(ruleDTO, id);

            if (HasNotifications())
            {
                _logger.LogWarning($"Houve um erro na atualizacao da regra: {id}");
                return Result();
            }

            _logger.LogInformation($"Regra atualizada com sucesso {id} - JSON: {JsonSerializer.Serialize(result)}");

            return Ok(new ResultViewModel
            {
                Message = "Registro atualizado com sucesso!",
                Success = true,
                Data = result
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveAsync(long id)
        {
            _logger.LogInformation($"Iniciando exclusao da regra {id}");

            await _ruleService.RemoveAsync(id);

            if (HasNotifications())
            {
                _logger.LogWarning($"Erro ao excluir a regra {id}");
                return Result();
            }

            _logger.LogInformation($"Regra excluida com sucesso {id}");

            return Ok(new ResultViewModel
            {
                Message = "Registro removido com sucesso!",
                Success = true,
                Data = null
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(long id)
        {
            _logger.LogInformation($"Iniciando consulta da regra: {id}");

            var rule = await _ruleService.GetAsync(id);

            if (HasNotifications())
            {
                _logger.LogWarning($"Houve um erro na consultar a regra: {id}");
                return Result();
            }

            _logger.LogInformation($"Regra consultada com sucesso: {id}");

            if (rule.Value == null)
            {
                _logger.LogWarning($"Nenhum regra foi encontrada com o ID: {id}");

                return Ok(new ResultViewModel
                {
                    Message = "Nenhum regra foi encontrado com o ID informado.",
                    Success = true,
                    Data = rule.Value
                });
            }

            _logger.LogInformation($"Regras encontradas com o ID: {id} - JSON: {JsonSerializer.Serialize(rule.Value)}");

            return Ok(new ResultViewModel
            {
                Message = "Regras encontrado com sucesso!",
                Success = true,
                Data = rule.Value
            });
        }

        [HttpGet]
        [Route("get-all")]
        public async Task<IActionResult> GetAsync()
        {
            _logger.LogInformation($"Iniciando consulta de todas as regras");

            var allRules = await _ruleService.GetAllAsync();

            if (HasNotifications())
            {
                _logger.LogWarning($"Nao foram encontradas regras");
                return Result();
            }

            _logger.LogInformation($"Regras encontradas com sucesso - {JsonSerializer.Serialize(allRules.Value)}");

            return Ok(new ResultViewModel
            {
                Message = "Regras encontradas com sucesso!",
                Success = true,
                Data = allRules.Value
            });
        }

    }
}
