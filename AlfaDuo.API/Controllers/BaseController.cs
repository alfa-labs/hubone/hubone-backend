﻿using AlfaDuo.API.ViewModels;
using AlfaDuo.Core.Communication.Handlers;
using AlfaDuo.Core.Communication.Messages.Notifications;
using AlfaDuo.Core.Enum;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AlfaDuo.API.Controllers
{
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
        private readonly DomainNotificationHandler _domainNotificationHandler;

        protected BaseController(
           INotificationHandler<DomainNotification> domainNotificationHandler)
        {
            _domainNotificationHandler = domainNotificationHandler as DomainNotificationHandler;
        }

        protected bool HasNotifications()
           => _domainNotificationHandler.HasNotifications();

        protected ObjectResult Created(dynamic responseObject)
            => StatusCode(201, responseObject);

        protected ObjectResult Result()
        {
            var notification = _domainNotificationHandler
                .Notifications
                .FirstOrDefault();

            return StatusCode(GetStatusCodeByNotificationType(notification.Type),
                new ResultViewModel
                {
                    Message = notification.Message,
                    Success = false,
                    Data = new { }
                });
        }

        private int GetStatusCodeByNotificationType(DomainNotificationType errorType)
        {
            return errorType switch
            {
                //Conflict
                DomainNotificationType.RecordAlreadyExists
                    => 409,

                //Unprocessable Entity
                DomainNotificationType.RecordInvalid
                    => 422,

                //Not Found
                DomainNotificationType.RecordNotFound
                    => 404,

                (_) => 500,
            };
        }
    }
}
