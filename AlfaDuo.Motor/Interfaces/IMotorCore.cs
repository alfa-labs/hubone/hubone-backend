﻿namespace AlfaDuo.Motor.Interfaces
{
    public interface IMotorCore
    {
        public Task<dynamic> ProcessByIntegration(long integration);
        public Task<dynamic> ProcessByRule(long ruleId);
    }
}