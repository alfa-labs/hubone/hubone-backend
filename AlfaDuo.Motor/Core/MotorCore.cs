﻿using AlfaDuo.Core.Interfaces;
using AlfaDuo.Domain.Configuration;
using AlfaDuo.Domain.Entities;
using AlfaDuo.Domain.Enum;
using AlfaDuo.Infra.Interfaces;
using AlfaDuo.Motor.Interfaces;
using AlfaDuo.Shared.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.ClearScript.V8;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySqlConnector;
using System.Dynamic;
using System.Linq.Expressions;
using System.Text;
using System.Text.Json;

namespace AlfaDuo.Motor.Core
{
    public class MotorCore : IMotorCore
    {
        private const string ENTITY_KEY = "entityKey";
        private const string FAUNA_INTERNAL = "__FAUNA__INTERNAL";

        private readonly IIntegrationRepository _integrationRepository;
        private readonly IPhaseRepository _phaseRepository;
        private readonly IMapper _mapper;
        private readonly IHanaAdapter _hana;
        private readonly IServiceLayerAdapter _serviceLayer;
        private readonly IRuleRepository _ruleRepository;
        private readonly IHttpAdapter _httpAdapter;
        private readonly ServiceLayer _servLayerConfig;
        private readonly Cache _cacheConfig;
        private readonly ILogger<MotorCore> _logger;
        private V8ScriptEngine _engine;
        private readonly IDistributedCache _distributedCache;

        public MotorCore(
            ILogger<MotorCore> logger,
            IOptions<Configuration> configurations,
            IPhaseRepository phaseRepository,
            IIntegrationRepository integrationRepository,
            IRuleRepository ruleRepository,
            IMapper mapper,
            IHanaAdapter hana,
            IServiceLayerAdapter serviceLayer,
            IHttpAdapter httpAdapter,
            IDistributedCache distributedCache)
        {
            _logger = logger;
            _integrationRepository = integrationRepository;
            _phaseRepository = phaseRepository;
            _ruleRepository = ruleRepository;
            _mapper = mapper;
            _hana = hana;
            _serviceLayer = serviceLayer;
            _httpAdapter = httpAdapter;
            _servLayerConfig = configurations.Value.ServiceLayer;
            _cacheConfig = configurations.Value.Cache;
            _distributedCache = distributedCache;
        }

        public async Task<dynamic> ProcessByRule(long ruleId)
        {
            try
            {
                _logger.LogInformation($"Iniciando o processamento da Regra: {ruleId}");

                var rule = await _getRule(ruleId);

                if (rule != null)
                {
                    var integration = await _generateIntegration(rule);

                    await ProcessByIntegration(integration.Id);
                }
                else
                {
                    _logger.LogWarning($"Consulta da Regra: {ruleId} nao retornou dados.");
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao processar o motor", ex);
                throw;
            }
        }

        private async Task<Integration> _generateIntegration(Rule rule)
        {
            var integration = new Integration
            {
                Payload = JsonSerializer.Serialize("{}"),
                QueryParam = JsonSerializer.Serialize("{}"),
                PathParam = JsonSerializer.Serialize("{}"),
                Status = IntegrationStatus.PENDING
            };

            var integrationCreated = await _integrationRepository.CreateAsync(integration);
            await _generateRules(integrationCreated.Id, rule);

            return integrationCreated;
        }

        public async Task<dynamic> ProcessByIntegration(long integrationId)
        {
            try
            {
                _logger.LogInformation($"Iniciando o processamento da Integracao ID: {integrationId}");

                var mapper = _configMapper();

                var integration = await _getIntegration(integrationId);
                var phase = await _getPhaseDetail(integrationId, 1);
                var notFinal = true;
                dynamic result = new { };

                await _changeState(integration, IntegrationStatus.PROCESSING);

                do
                {
                    if (phase != null)
                    {
                        var payload = integration.Payload;
                        var pathParameter = integration.PathParam;
                        var queryParameter = integration.QueryParam;
                        var rule = JsonSerializer.Deserialize<RuleDetailDto>(phase.Rule);

                        result = await _processPhase(phase, rule, integrationId, payload, pathParameter, queryParameter);

                        if (rule.NextSequence != null)
                        {
                            if (result != null)
                                if (rule.NextSequence.SequenceIfOk == 0)
                                {
                                    notFinal = false;
                                    phase = null;
                                }
                                else phase = await _getPhaseDetail(integrationId, rule.NextSequence.SequenceIfOk);
                            else
                            {
                                if (rule.NextSequence.SequenceIfNotOk == 0)
                                {
                                    notFinal = false;
                                    phase = null;
                                }
                                else
                                {
                                    phase = await _getPhaseDetail(integrationId, rule.NextSequence.SequenceIfNotOk);

                                }
                            }
                        }
                        else
                        {
                            notFinal = false;
                            phase = null;
                        }
                    }
                    else
                    {
                        notFinal = false;
                    }

                } while (notFinal);

                await _changeState(integration, IntegrationStatus.FINISHED);

                _logger.LogInformation($"Finalizado o processamento da Integracao ID: {integrationId}");

                return JsonSerializer.Deserialize<dynamic>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao processar o motor", ex);
                throw;
            }
        }

        private async Task<string> _processPhase(
            Phase phase,
            RuleDetailDto rule,
            long integrationId,
            string payload,
            string pathParameter,
            string queryParameter)
        {
            var type = rule.Type;
            dynamic result = "";

            switch (type)
            {
                case "1":
                    result = await _executeQueryInHana(rule.Query, integrationId, payload, pathParameter, queryParameter);
                    break;
                case "2":
                    result = await _executeServiceLayer(rule.ServiceLayer, integrationId, payload, pathParameter, queryParameter);
                    break;
                case "3":
                    result = await _executeCallToRestAPI(rule.Rest, integrationId, payload, pathParameter, queryParameter);
                    break;
            }

            if (rule.ResultFormat != null && rule.ResultFormat.Count > 0)
                result = await _formatResult(rule.ResultFormat, result);

            phase.setPhaseResult(result);
            await _phaseRepository.UpdateAsync(phase);

            return result;
        }

        private async Task<Integration> _getIntegration(long integrationId)
        {
            Expression<Func<Integration, bool>> filter = integration
               => integration.Id == integrationId;

            var integration = await _integrationRepository.GetAsync(filter);

            return integration;
        }

        private async Task<Phase> _getPhaseDetail(long integrationId, int sequence)
        {
            Expression<Func<Phase, bool>> filter = phase
               => phase.IntegrationId == integrationId
               && phase.Sequence == sequence;

            var phase = await _phaseRepository.GetAsync(filter);

            return phase;
        }

        private async Task<Integration> _changeState(Integration integration, IntegrationStatus status)
        {
            integration.SetStatus(status);

            var updated = await _integrationRepository.UpdateAsync(integration);

            return updated;
        }

        private async Task<string> _executeServiceLayer(ServiceLayerDetail serviceLayer, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            var urlParams = String.Empty;
            var jsonIterate = String.Empty;
            var bodyParametersRule = serviceLayer.BodyParameter;
            var pathParametersRule = serviceLayer.PathParameters;
            var queryParametersRule = serviceLayer.QueryParameters;
            var entityKeyRule = serviceLayer.EntityKey;

            object payloadServiceLayer = new { };

            if (entityKeyRule != null)
            {
                var entityKey = await _getEntityKey(entityKeyRule, integrationId, payload, pathParameter, queryParameter);

                if (!String.IsNullOrEmpty(entityKey))
                {
                    urlParams = $"({entityKey})";
                }
            }

            if (bodyParametersRule != null && bodyParametersRule.Fields.Count > 0)
            {
                payloadServiceLayer = await getParamsDictionary(bodyParametersRule, integrationId, payload, pathParameter, queryParameter);
            }

            if (pathParametersRule != null)
            {
                var pathParam = await _getPathParam(pathParametersRule, integrationId, payload, pathParameter, queryParameter);

                if (!String.IsNullOrEmpty(pathParam))
                {
                    urlParams += $"/{pathParam}";
                }
            }

            if (queryParametersRule != null)
            {
                var queryParam = await _getQueryParam(queryParametersRule, integrationId, payload, pathParameter, queryParameter);

                if (!String.IsNullOrEmpty(queryParam))
                {
                    urlParams += $"?{queryParam}";
                }
            }

            object resultServiceLayer = new { };

            switch (serviceLayer.Verb.ToLower())
            {
                case "get":
                    resultServiceLayer = await _serviceLayer.Call<dynamic>(
                        $"{serviceLayer.Endpoint}{urlParams}",
                        HttpMethod.Get,
                        _servLayerConfig.Uri);
                    break;
                case "post":
                    resultServiceLayer = await _serviceLayer.Call<dynamic>(
                        $"{serviceLayer.Endpoint}{urlParams}",
                        HttpMethod.Post,
                        _servLayerConfig.Uri,
                        payloadServiceLayer);
                    break;
                case "patch":
                    resultServiceLayer = await _serviceLayer.Call<dynamic>(
                        $"{serviceLayer.Endpoint}{urlParams}",
                        HttpMethod.Patch,
                        _servLayerConfig.Uri,
                        payloadServiceLayer);
                    break;
                case "put":
                    resultServiceLayer = await _serviceLayer.Call<dynamic>(
                        $"{serviceLayer.Endpoint}{urlParams}",
                        HttpMethod.Put,
                        _servLayerConfig.Uri,
                        payloadServiceLayer);
                    break;
                case "delete":
                    resultServiceLayer = await _serviceLayer.Call<dynamic>(
                        $"{serviceLayer.Endpoint}{urlParams}",
                        HttpMethod.Delete,
                        _servLayerConfig.Uri);
                    break;
            }

            return JsonSerializer.Serialize(resultServiceLayer);
        }

        private async Task<string> _executeQueryInHana(QueryDetail query, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            var sql = query.Sql;
            var parameters = query.Parameters;

            if (parameters != null)
            {
                var sqlValues = await _getSQLDictionary(parameters, integrationId, payload, pathParameter, queryParameter);
                int idx = 0;
                foreach (var parameter in sqlValues)
                {
                    sql = sql.Replace("{" + idx + "}", parameter.ToString());
                    idx++;
                }
            }

            dynamic sqlResult = new { };

            if (query.Execute) sqlResult = await _hana.Execute(sql);
            else sqlResult = await _hana.Query<dynamic>(sql);
            
            if (sqlResult.Count > 0) 
                return JsonSerializer.Serialize(sqlResult);

            return null;
        }

        private async Task<dynamic> _executeCallToRestAPI(RestDetail? rest, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            var urlParams = "";
            var bodyParametersRule = rest.BodyParameter;
            var pathParametersRule = rest.PathParameters;
            var queryParametersRule = rest.QueryParameters;
            var headerRule = rest.Headers;
            var authRule = rest.Auth;

            List<dynamic> jsonIterate;
            Dictionary<string, string> headers = new Dictionary<string, string>();

            if (pathParametersRule != null && pathParametersRule.Count > 0)
            {
                var pathParam = await _getPathParam(pathParametersRule, integrationId, payload, pathParameter, queryParameter);
                if (!String.IsNullOrEmpty(pathParam))
                {
                    urlParams += $"{pathParam}";
                }
            }

            if (queryParametersRule != null && queryParametersRule.Count > 0)
            {
                var queryParam = await _getQueryParam(queryParametersRule, integrationId, payload, pathParameter, queryParameter);

                if (!String.IsNullOrEmpty(queryParam))
                {
                    urlParams += $"?{queryParam}";
                }
            }

            if (headerRule != null && headerRule.Count > 0)
            {
                foreach (var header in headerRule)
                {
                    headers.Add(header.Key, header.Value);
                }
            }

            if (authRule != null)
            {
                if (authRule.Type == "bearer")
                {
                    var authKey = authRule.InternalAuthKey;
                    var cache = await _getCacheItemFromDatabaseAsync(authKey);
                    var utcNow1 = new SystemClock();
                    DateTimeOffset utcNow = utcNow1.UtcNow;

                    if (cache != null)
                    {
                        if (DateTime.Compare(cache.ExpiresAtTime.DateTime, utcNow.ToLocalTime().DateTime) < 0)
                        {
                            await _distributedCache.RemoveAsync(authKey);
                            cache = null;
                        }
                    }

                    if (cache == null)
                    {
                        var payloadAuth = await getParamsAuth(authRule.BodyParameters);
                        dynamic resultAuth;

                        resultAuth = await _httpAdapter.Call<ExpandoObject>(
                            HttpMethod.Post,
                            authRule.BaseUrl,
                            authRule.EndPoint,
                            headers,
                            payloadAuth);

                        int secondsOfRequest = Int32.Parse(resultAuth.expires_in.ToString());

                        byte[] access_bytes = Encoding.ASCII.GetBytes(resultAuth.access_token.ToString());

                        await _distributedCache.SetAsync(authKey,
                                                    access_bytes,
                                                    new DistributedCacheEntryOptions().SetAbsoluteExpiration(absolute: utcNow.ToLocalTime()
                                                                                                             .AddSeconds(secondsOfRequest)));
                    }
                }
            }

            dynamic resultRest = new { };

            if (bodyParametersRule != null && bodyParametersRule.Fields.Count > 0)
            {
                if (bodyParametersRule.Iterate)
                {
                    var stringJsonIterate = await _getJsonResultByOrigin(bodyParametersRule.OriginIterate, integrationId, payload, pathParameter, queryParameter);
                    jsonIterate = JsonSerializer.Deserialize<List<dynamic>>(stringJsonIterate);
                    resultRest = new List<dynamic>();

                    foreach (var item in jsonIterate)
                    {
                        var itemIterate = JsonSerializer.Serialize(item);
                        var payloadRest = await getParamsDictionary(bodyParametersRule, integrationId, payload, pathParameter, queryParameter, itemIterate, bodyParametersRule.OriginIterate);
                        var resultCall = await _callApi(rest.Verb, rest.BaseUrl, $"{rest.Endpoint}{urlParams}", headers, payloadRest);

                        resultRest.Add(resultCall);
                    }
                }
                else
                {
                    var payloadRest = await getParamsDictionary(bodyParametersRule, integrationId, payload, pathParameter, queryParameter);
                    resultRest = await _callApi(rest.Verb, rest.BaseUrl, $"{rest.Endpoint}{urlParams}", headers, payloadRest);
                }
            }

            return JsonSerializer.Serialize(resultRest);
        }

        private async Task<object> _callApi(string verb, string baseUrl, string fullPath, Dictionary<string, string> headers, object payload)
        {
            object resultRest = new { };

            switch (verb.ToLower())
            {
                case "get":
                    resultRest = await _httpAdapter.Call<dynamic>(
                        HttpMethod.Get,
                        baseUrl,
                        fullPath,
                        headers);
                    break;
                case "post":
                    resultRest = await _httpAdapter.Call<dynamic>(
                        HttpMethod.Post,
                        baseUrl,
                        fullPath,
                        headers,
                        payload);
                    break;
                case "patch":
                    resultRest = await _httpAdapter.Call<dynamic>(
                        HttpMethod.Patch,
                        baseUrl,
                        fullPath,
                        headers,
                        payload);
                    break;
                case "put":
                    resultRest = await _httpAdapter.Call<dynamic>(
                        HttpMethod.Put,
                        baseUrl,
                        fullPath,
                        headers,
                        payload);
                    break;
                case "delete":
                    resultRest = await _httpAdapter.Call<dynamic>(
                        HttpMethod.Delete,
                        baseUrl,
                        fullPath,
                        headers);
                    break;
            }

            return resultRest;
        }

        private async Task<List<dynamic>> _getSQLDictionary(List<QueryParameters> parameters, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            dynamic result = new { };
            List<dynamic> listSQLParam = new List<dynamic>();

            parameters = parameters.OrderBy(param => param.Order).ToList();

            foreach (QueryParameters parameter in parameters)
            {
                var field = parameter.Order.ToString();
                var value = parameter.Value.ToString();
                var origin = parameter.Origin;
                var jsoncontent = await _getJsonResultByOrigin(origin, integrationId, payload, pathParameter, queryParameter);

                result = await _getValue(jsoncontent, value, field, result);
            }

            var jsResult = _engine.Script.GetObjectValues(JsonSerializer.Serialize(result));
            var sqlValues = JsonSerializer.Deserialize<List<dynamic>>(jsResult);
            return sqlValues;

        }

        private async Task<object> getParamsDictionary(
                                                        BodyParameterDetail bodyParameter,
                                                        long integrationId,
                                                        string payload,
                                                        string pathParameter,
                                                        string queryParameter,
                                                        string? itemIterate = null,
                                                        int? sequenceIterate = null)
        {
            dynamic result = new { };
            var parameters = bodyParameter.Fields;

            foreach (ParametersDetail parameter in parameters)
            {
                var field = parameter.Field;
                var value = parameter.Value.ToString();
                var origin = parameter.Origin;
                string jsonContent = "";
                var expando = new ExpandoObject() as IDictionary<string, object>;

                if (sequenceIterate != null && sequenceIterate == origin)
                {
                    jsonContent = itemIterate;
                }
                else if (origin != -1)
                {
                    jsonContent = await _getJsonResultByOrigin(origin, integrationId, payload, pathParameter, queryParameter);
                }
                else if (origin == -1 && value == "")
                {
                    value = FAUNA_INTERNAL;
                    expando.Add(FAUNA_INTERNAL, "");

                    jsonContent = JsonSerializer.Serialize(expando);
                }
                else if (origin == -1 && value != "")
                {
                    expando.Add(FAUNA_INTERNAL, value);
                    value = FAUNA_INTERNAL;

                    jsonContent = JsonSerializer.Serialize(expando);
                }

                result = await _getValue(jsonContent, value, field, result);
            }

            var values = result;

            return values;
        }

        private async Task<object> getParamsAuth(List<AuthDetailValues> parameters)
        {
            dynamic result = new { };

            foreach (AuthDetailValues parameter in parameters)
            {
                var field = parameter.Field;
                var value = parameter.Value.ToString();
                string jsonContent = "";
                var expando = new ExpandoObject() as IDictionary<string, object>;

                if (value == "")
                {
                    value = FAUNA_INTERNAL;
                    expando.Add(FAUNA_INTERNAL, "");

                    jsonContent = JsonSerializer.Serialize(expando);
                }
                else
                {
                    expando.Add(FAUNA_INTERNAL, value);
                    value = FAUNA_INTERNAL;

                    jsonContent = JsonSerializer.Serialize(expando);
                }

                result = await _getValue(jsonContent, value, field, result);
            }

            var values = result;

            return values;
        }

        private async Task<dynamic> _getValue(string jsonContent, dynamic value, string field, dynamic result)
        {
            string jsResult = _engine.Script.GetValue(jsonContent, field, value, JsonSerializer.Serialize(result)).ToString();
            result = JsonSerializer.Deserialize<dynamic>(jsResult);

            return result;
        }

        private async Task<string> _getJsonResultByOrigin(
            int origin,
            long integrationId,
            string payload,
            string pathParam,
            string queryParam)
        {
            string jsonResult;

            if (origin == 0) jsonResult = payload;
            else if (origin == -2) jsonResult = queryParam;
            else if (origin == -3) jsonResult = pathParam;
            else
            {
                Expression<Func<Phase, bool>> filter = integration
               => integration.Id == integrationId;

                var phase = await _getPhaseDetail(integrationId, origin);
                jsonResult = phase.PhaseResult;
            }

            return jsonResult;
        }

        private async Task<dynamic> _formatResult(List<ResultFormatDetail> resultFormat, string jsonResult)
        {
            dynamic newFormat = new { };

            foreach (var format in resultFormat)
            {
                var field = format.Field;
                var value = format.Value.ToString();

                newFormat = await _getValue(jsonResult, value, field, newFormat);
            }
            return JsonSerializer.Serialize(newFormat);
        }

        private async Task<string> _getEntityKey(EntityKeyDetail entityKeyRule, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            var origin = entityKeyRule.Origin;
            var value = entityKeyRule.Value;
            var jsonContent = await _getJsonResultByOrigin(origin, integrationId, payload, pathParameter, queryParameter);
            var entityValue = await _getValue(jsonContent, value, ENTITY_KEY, new { });

            var entityKey = _engine.Script.GetPropertyValue(JsonSerializer.Serialize(entityValue), ENTITY_KEY);
            return $"'{entityKey}'";
        }

        private async Task<string> _getQueryParam(List<ParametersDetail> parametersKeyRule, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            var queryStringRet = "";
            foreach (var format in parametersKeyRule)
            {
                var origin = format.Origin;
                var field = format.Field;
                var value = format.Value.ToString();
                var jsonContent = "";
                var expando = new ExpandoObject() as IDictionary<string, object>;

                if (origin != -1)
                {
                    jsonContent = await _getJsonResultByOrigin(origin, integrationId, payload, pathParameter, queryParameter);
                }
                else if (origin == -1 && value == "")
                {
                    value = FAUNA_INTERNAL;
                    expando.Add(FAUNA_INTERNAL, "");

                    jsonContent = JsonSerializer.Serialize(expando);
                }
                else if (origin == -1 && value != "")
                {
                    expando.Add(FAUNA_INTERNAL, value);
                    value = FAUNA_INTERNAL;

                    jsonContent = JsonSerializer.Serialize(expando);
                }

                var queryStringItem = await _getValue(jsonContent, value, field, new { });
                var queryStringValue = _engine.Script.GetPropertyValue(JsonSerializer.Serialize(queryStringItem), field);

                queryStringRet += $"{field}={queryStringValue}&";
            }

            if (!String.IsNullOrEmpty(queryStringRet)) queryStringRet = queryStringRet.Substring(0, queryStringRet.Length - 1);

            return queryStringRet;
        }

        private async Task<string> _getPathParam(List<ParametersPathDetail> parametersRule, long integrationId, string payload, string pathParameter, string queryParameter)
        {
            dynamic result = new { };
            var pathStringRet = "";
            parametersRule = parametersRule.OrderBy(param => param.Order).ToList();

            foreach (var format in parametersRule)
            {
                var origin = format.Origin;
                var field = format.Order.ToString();
                var value = format.Value.ToString();
                var jsoncontent = await _getJsonResultByOrigin(origin, integrationId, payload, pathParameter, queryParameter);

                result = await _getValue(jsoncontent, value, field, result);
            }

            var jsResult = _engine.Script.GetObjectValues(JsonSerializer.Serialize(result));
            var pathValues = JsonSerializer.Deserialize<List<dynamic>>(jsResult);

            foreach (var pathValue in pathValues)
            {
                pathStringRet += $"/{pathValue}";
            }

            return pathStringRet;
        }

        private string _configMapper()
        {
            var mapper = File.ReadAllText("scripts/dictionary-mapper.js");
            _engine = new V8ScriptEngine();
            _engine.Execute(mapper);

            _engine.AddHostType("Console", typeof(Console));

            return mapper;
        }

        private async Task _generateRules(long id, Rule layout)
        {
            var rules = JsonSerializer.Deserialize<List<RuleDetailDto>>(layout.Rules);

            foreach (var rule in rules)
            {
                var phase = new Phase();
                phase.setSequence(rule.Sequence);
                phase.setRule(JsonSerializer.Serialize(rule));
                phase.setType("2");
                phase.setIntegrationId(id);

                await _phaseRepository.CreateAsync(phase);
            }
        }

        private async Task<Rule> _getRule(long id)
        {
            Expression<Func<Rule, bool>> filter = rule
           => rule.Id == id
           && rule.Active;

            var rule = await _ruleRepository.GetAsync(filter);

            return rule;
        }

        private async Task<CacheItemInfo> _getCacheItemFromDatabaseAsync(string key)
        {
            using (var connection = new MySqlConnection(_cacheConfig.Auth.ConnectionString))
            {
                var command = new MySqlCommand(
                    "SELECT Id, Value, ExpiresAtTime, SlidingExpirationInSeconds, AbsoluteExpiration " +
                    $"FROM {_cacheConfig.Auth.SchemaName}.{_cacheConfig.Auth.TableName} WHERE Id = @Id",
                    connection);

                command.Parameters.AddWithValue("Id", key);

                await connection.OpenAsync();

                var reader = await command.ExecuteReaderAsync(System.Data.CommandBehavior.SingleRow);

                // NOTE: The following code is made to run on Mono as well because of which
                // we cannot use GetFieldValueAsync etc.
                if (await reader.ReadAsync())
                {
                    var cacheItemInfo = new CacheItemInfo
                    {
                        Id = key,
                        Value = (byte[])reader[1],
                        ExpiresAtTime = DateTimeOffset.Parse(reader[2].ToString(), System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat,
                        System.Globalization.DateTimeStyles.AssumeUniversal)
                    };

                    if (!await reader.IsDBNullAsync(3))
                    {
                        cacheItemInfo.SlidingExpirationInSeconds = TimeSpan.FromSeconds(reader.GetInt64(3));
                    }

                    if (!await reader.IsDBNullAsync(4))
                    {
                        cacheItemInfo.AbsoluteExpiration = DateTimeOffset.Parse(reader[4].ToString(), System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat,
                            System.Globalization.DateTimeStyles.AssumeUniversal);
                    }

                    return cacheItemInfo;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}